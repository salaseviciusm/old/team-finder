<?php

	/* Require main connection file */
	require 'config.php';

	/* Get id of the page requested */
	$id = $_GET['id'];

	/* Check if user is logged in */
	if(empty($_SESSION['username'])){
		header('Location: index.php');
	}

	/* Check if room is not empty */
	if($id == '') {
		header('Location: index.php');
	}

	if(isset($_GET['id'])) {
		try {
			$stmt = $connect->prepare('SELECT * FROM channels INNER JOIN games ON channels.gameID = games.game_id INNER JOIN users ON channels.author = users.user_id WHERE id = :id');
			$stmt->execute(array(
				':id' => $id
				));
			$data = $stmt->fetch(PDO::FETCH_ASSOC);

			if($data['chosenTime'] == 168) {
				$time = "7d";
			} else {
				$time = $data['chosenTime']."h";
			}

			$name = $data['name'];
			$creator = $data['username'];
			$dcts = $data['dcts'];
			$adult = $data['adult'];
			$icon = $data['icon'];
			$mic = $data['mic'];
			$discord = "<div class='option'><i class='fab fa-discord'></i> ".$data['discord']."</div>";
			$skype = "<div class='option'><i class='fab fa-skype'></i> ".$data['skype']."</div>";
			$steam = "<div class='option'><i class='fab fa-steam'></i> ".$data['steam']."</div>";

			if($data['discord'] == '') {
				$discord = "";
			}

			if($data['skype'] == '') {
				$skype = "";
			}

			if($data['steam'] == '') {
				$steam = "";
			}

			if($data == false){
				header("Location: index.php");
			}
		} catch(PDOException $e) {
			$err = $e->getMessage();
		}
	}

?>

<?php include "core/header.php"; ?>

	<div class='container'>
		<section id='content'>

		<div class='channel-info'>
				<div class='request'>
					<h4><img src='<?php echo $icon; ?>' alt=''/><?php echo $name; ?></h4>
					<div class='status'>Expires in: <?php echo $time; ?></div>
					<div class='creator'>Created by : <?php echo $creator; ?></div>

					<?php echo $discord; ?>
					<?php echo $skype; ?>
					<?php echo $steam; ?>
				</div>
			</div>

			<div class='message'>

			</div>

			<div class='clear'></div>

			<form method="post" class="cd-form" id='sendMessage'>

				<p class="fieldset">
					<label class="image-replace cd-username" for="signup-username">Message text...</label>
					<input name='message' class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Message text..."  autocomplete="off">
				</p>

				<p class="fieldset">
					<input name='send' class="full-width blue" type="submit" value="Send">
				</p>

				<span class="message-send"></span>
			</form>



		</section>
	</div>

	<!-- Javascript to update the chat -->

	<script language="javascript" type="text/javascript">

(function message() {
  $.ajax({
    url: 'updateMessages.php?id=<?php echo $id; ?>',
    success: function(data) {
      $('.message').html(data);
    },
    complete: function() {
      // Schedule the next request when the current one's complete
      setTimeout(message, 100);
    }
  });
})();
</script>

	<script>
			$(document).on('submit', '#sendMessage', function() {



				$.post('send.php?id=<?php echo $id; ?>', $(this).serialize(), function(data) {
					$(".message-send").html(data);

					if(data === "<div class='hidden-success'>Success</div>"){
						$("#sendMessage").trigger('reset');
					}
				});
				return false;
			});
		  </script>


<?php include "core/footer.php"; ?>
