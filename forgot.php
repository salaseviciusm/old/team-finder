<?php

/* Require main connection file */
require "config.php";

/* Check if user is logged in */
if(isset($_SESSION['username'])){
   header("Location:index.php");
}

if(!isset($_POST['email'])){
	header("Location:index.php");
}

/* Set input data into variables */

$email = $_POST['email'];
$response = $_POST["g-recaptcha-response"];


/* Do validation */
if($email == '') {
	echo 'Please enter your email address!';
} else {
	if($response == ''){
		echo '<span class="message">Complete captcha!</span>';
	} else {

    /* Check if email address exist and send email with link and generated token otherwise show error message */
		try {
			$stmt = $connect->prepare('SELECT password, email FROM users WHERE email = :email');
			$stmt->execute(array(
				'email' => $email
				));
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			if($email == $data['email']) {

				$token = bin2hex(openssl_random_pseudo_bytes(16));

				$stmt = $connect->prepare('UPDATE users SET token = :token WHERE email = :email');
				$stmt->execute(array(
					'token' => $token,
					'email' => $email
				));

				$subject = 'Password reset';

				$messageFull = "
					To reset your password please go to the link below:<br>
					www.mantas.dev/team-finder/reset.php?token=".$token."
				";

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: TEAM-FINDER <team-finder@gmail.com>' . "\r\n";

				mail($email, $subject, $messageFull, $headers);

				echo "<span>Success. Please check your email for further instructios!</span>";

			}
			else {
				echo 'Email address do not exist!';
			}
		}
		catch(PDOException $e) {
			$err = $e->getMessage();
		}
	}
}

?>
