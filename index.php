<?php

/* Require main connection file */
require 'config.php';

/* Set up expiry for channels */
$content ='';

	try {

		$stmt = $connect->prepare('DELETE FROM channels WHERE created < DATE_SUB(NOW(), INTERVAL 1 HOUR) && chosenTime = 1');
		$stmt->execute(); //And bind the values

		$stmt = $connect->prepare('DELETE FROM channels WHERE created < DATE_SUB(NOW(), INTERVAL 6 HOUR) && chosenTime = 6');
		$stmt->execute(); //And bind the values

		$stmt = $connect->prepare('DELETE FROM channels WHERE created < DATE_SUB(NOW(), INTERVAL 12 HOUR) && chosenTime = 12');
		$stmt->execute(); //And bind the values

		$stmt = $connect->prepare('DELETE FROM channels WHERE created < DATE_SUB(NOW(), INTERVAL 24 HOUR) && chosenTime = 24');
		$stmt->execute(); //And bind the values

		$stmt = $connect->prepare('DELETE FROM channels WHERE created < DATE_SUB(NOW(), INTERVAL 168 HOUR) && chosenTime = 168');
		$stmt->execute(); //And bind the values

		$vars = array('dcts', 'adult', 'mic', 'gameID');
		$query = "SELECT * FROM channels INNER JOIN games ON channels.gameID = games.game_id INNER JOIN users ON channels.author = users.user_id ";
		$filter = array();
		$binds = array();

		foreach($vars as $key => $value) {
			if ($_POST[$value] != '') {
				$filter[] = $value." = :".$value;
				$binds[':'.$value] = $_POST[$value];
			}
		}

		if (!empty($filter)) {
			$query .= " WHERE ".implode(' AND ', $filter);
		}

		$query .= " ORDER BY channels.id DESC";

		$stmt = $connect->prepare($query);
		$stmt->execute($binds);
		$data = $stmt->fetchAll();

		/* Check if its hours or days. */
		if ( !empty( $data ) ) {
			foreach ($data as $channel) {

				if($channel['chosenTime'] == 168) {
					$time = "7d";
				} else {
					$time = $channel['chosenTime']."h";
				}

				/* If user logged in allow access to the room, otherwise show login/register pop up. */

				if(isset($_SESSION['username'])){
					$link = "room.php?id=".$channel['id'];
					$link = "<a href='room.php?id=".$channel['id']."' class='go-to-room'><i class='fas fa-plug'></i> Connect to the channel</a>";
					$button_text = "Conncet to the channel";
				} else {
					$link = "<div class='main-nav'><a href='#' class='go-to-room cd-signin'><i class='fas fa-plug'></i> Login/Register</a></div>";
				}

				/* Display content from database, otherwise show no message */

				$content = $content."
				<div class='col-4'>
					<div class='request'>
						<h4><img src='".$channel['icon']."' alt=''/>".$channel['name']."</h4>
						<div class='status'>Expires in: ".$time."</div>
						<div class='creator'>Created by : ".$channel['username']."</div>
						<div class='option ".$channel['dcts']."'><i class='fas fa-check'></i> Discord/Teamspeak needed</div>
						<div class='option ".$channel['adult']."'><i class='fas fa-check'></i> From 18+ or above</div>
						<div class='option ".$channel['mic']."'><i class='fas fa-check'></i> Microphone needed</div>

						".$link."
					</div>
				</div>
				";
			}
		} else {
		  $content = "<div style='color: #fff;font-size: 14px;text-align: center;padding: 150px 20px;'>There are no active channels!</div>";
		}

	} catch(PDOException $e) {
		echo $e->getMessage();
	}

?>

<?php include "core/header.php"; ?>

	<!-- Filter bit -->

	<div class='container'>
		<div class='filter'>
			<h4><i class="fas fa-filter"></i> Filter</h4>
			<div class='filter-content'>
			<form method="post" id='filter-form'>
				<select name='gameID'>
				<option selected  disabled>Select game</option>
				<?php
					try {
						$stmt = $connect->prepare('SELECT * FROM games');
						$stmt->execute();
						$data = $stmt->fetchAll();

						foreach ($data as $game){
							echo '<option style="background-image:url('.$game['icon'].');" value="'.$game['game_id'].'">'.$game['name'].'</option>';
						}

					} catch(PDOException $e) {
						$err = $e->getMessage();
					}
				?>
				</select>

				<span><input type="checkbox" name="dcts" value="yes"> Discord/Teamspeak needed</span>
				<span><input type="checkbox" name="adult" value="yes"> From 18+ or above</span>
				<span><input type="checkbox" name="mic" value="yes"> Microphone needed</span>

				<a href='#' class='button right blue search' onClick="document.forms['filter-form'].submit();">
					<span><i class="fas fa-search"></i></span>
                    <p class='search'>Filter</p>
				</a>
				</form>
			 	<div class='clear'></div>
			</div>
		</div>
	</div>

	<div id='mainContent'>
		<div class='container'>
            <?php if(isset($_SESSION['username'])){ ?>
                <span class='main-nav'>
                    <a href="#" class='button left purple login'>
                        <span><i class="fas fa-plus"></i></span>
                        <p class='cd-add'>Add channel</p>
                    </a>
                </span>

                <div class='clear'></div>

                <br>
            <?php }?>

            <div class='row'>
                <?php echo $content; ?>
                <div class='clear'></div>
            </div>
		</div>
	</div>

<?php include "core/footer.php"; ?>
