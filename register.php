<?php
/* Require main connection file */
require 'config.php';

/* Make sure file is not accessible using the link to the page as its only used in ajax calls */

if(isset($_SESSION['username'])){
   header("Location:index.php");
}

if(!isset($_POST['username'])){
	header("Location:index.php");
}

	// Get data

	$username = $_POST['username'];
	$password = $_POST['password'];
	$rpassword = $_POST['rpassword'];
	$email = $_POST['email'];
	$hash = password_hash($password, PASSWORD_DEFAULT);
	$response = $_POST["g-recaptcha-response"];
    $role = "user";


	if($username == '' || $password == '' || $rpassword == '' || $email == '') {
		echo 'Complete all fields!';
	} else {
		if($password == $rpassword) {
			if($response == ''){
				echo 'Complete captcha!';
			} else {
				try {
					$stmt = $connect->prepare('INSERT INTO users (username, email, password, role) VALUES (:username, :email, :password, :role)');
					$stmt->execute(array(
						':username' => $username,
						':email' => $email,
						':password' => $hash,
            'role' => $role
						));

            $stmt = $connect->prepare('SELECT user_id FROM users WHERE username = :username');
            $stmt->execute(array(
                ':username' => $username
                ));
            $data = $stmt->fetch(PDO::FETCH_ASSOC);

					$_SESSION['memberid'] = $data['user_id'];
          $_SESSION['role'] = $role;
          $_SESSION['username'] = $username;
          $_SESSION['email'] = $email;
					$_SESSION['password'] = $hash;

					echo "<div class='hidden-success'>Success</div>";
				}
				catch(PDOException $e) {
					$err = $e->getMessage();
				}
			}
		} else {
			echo 'Passwords are different!';
		}
	}
?>
