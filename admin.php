<?php

/* Require main connection file */
require 'config.php';

/* Check if user is logged in */
if(empty($_SESSION['username'])){
    header('Location: login.php');
}

/* Check if user has admin role */
if($_SESSION['role'] != 'admin'){
    header('Location: login.php');
}


/* Validation before adding the category */
if(isset($_POST['addGame'])) {

        $gameName = $_POST['gameName'];
        $icon = $_POST['icon'];

        if($gameName == '' || $icon == '' ) {
            $err = 'Complete all fields!';
        } else {

          /* Add game category to the database */
            try {
                $stmt = $connect->prepare('INSERT INTO games (name, icon) VALUES (:name, :icon)');
                $stmt->execute(array(
                    'name' => $gameName,
                    'icon' => $icon
                    ));

                $err = "Success! Game has been added.";
            }
            catch(PDOException $e) {
                $err = $e->getMessage();

            }
        }
    }
?>

<?php include "core/header.php"; ?>

<div class='container'>
    <section id='content'>
        <h4>Welcome back:  <span><?php echo $_SESSION['username']; ?> (<?php echo $_SESSION['role']; ?>)</span></h4>

        <div class='col-2'>

        </div>

            <h3>Add game</h3>

            <form method="post" class="cd-form" id='addGame-form'>
                <p class="fieldset">
                    <label class="image-replace cd-username" for="signup-username">Game name</label>
                    <input name='gameName' class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Game name"  autocomplete="off">
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-username" for="signup-username">Game icon (http://..)</label>
                    <input name='icon' class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Game icon (http://..)"  autocomplete="off">
                </p>

                <p class="fieldset">
                    <input name='addGame' class="full-width blue" type="submit" value="Pridėti">
                </p>

                <span class="message-update"><?php echo $err; ?></span>
            </form>

    </section>
</div>

<?php include "core/footer.php"; ?>
