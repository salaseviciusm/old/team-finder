﻿<?php

/* Require main connection file */
require 'config.php';

/* Set input data into variables */
$gameID = $_POST['gameID'];
$dcts = $_POST['dcts'];
$adult = $_POST['adult'];
$mic = $_POST['mic'];
$authorID = $_SESSION['memberid'];
$response = $_POST["g-recaptcha-response"];
$chosenTime = $_POST['chosenTime'];
$discord = $_POST['discord'];
$skype = $_POST['skype'];
$steam = $_POST['steam'];

/* Do validation */

if($gameID == ''){
	echo 'Choose game!';
} else {
	if($chosenTime == ''){
		echo 'Select expiry time';
	} else {
		if($response == ''){
			echo 'Complete captcha!';
		} else {

			/* Add game to the database */
			try {
				$stmt = $connect->prepare('INSERT INTO channels (dcts, adult, mic, gameID, author, chosenTime, discord, skype, steam) VALUES (:dcts, :adult, :mic, :gameID, :authorID, :chosenTime, :discord, :skype, :steam)');
				$stmt->execute(array(
					'dcts' => $dcts,
					'adult' => $adult,
					'mic' => $mic,
					'gameID' => $gameID,
					'authorID' => $authorID,
					'chosenTime' => $chosenTime,
					'discord' => $discord,
					'skype' => $skype,
					'steam' => $steam
					));

				echo "<div class='hidden-success'>Success</div>";

			}
			catch(PDOException $e) {
				echo $e->getMessage();
			}
		}
	}
}

?>
