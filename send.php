<?php

/* Require main connection file */
require 'config.php';

/* Get rooms id */
$id = $_GET['id'];

	/* Check if user is logged in */
	if(empty($_SESSION['username'])){
		header('Location: index.php');
	}

	/* Check if post is sent, otherwise don't allow access as this page only used in ajax calls */
	if(!isset($_POST['message'])){
		header('Location: index.php');
	}

	if($id == '') {
		header('Location: index.php');
	}

	if(isset($_GET['id'])) {
		try {
			$stmt = $connect->prepare('SELECT * FROM channels WHERE id = :id');
			$stmt->execute(array(
				':id' => $id
				));
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			if($data == false){
				header("Location: index.php");
			}
		} catch(PDOException $e) {
			$err = $e->getMessage();
		}


			$poster = $_SESSION['memberid'];
			$message = $_POST['message'];

			date_default_timezone_set("Europe/London");
			$date = date("H:i:s");

			if($message == '') {
				echo 'Please enter your message!';
			} else {
				try {
					$stmt = $connect->prepare('INSERT INTO messages (poster_id, convo_id, message, date ) VALUES (:poster_id, :convo_id, :message, :date )');
					$stmt->execute(array(
						'poster_id' => $poster,
						'convo_id' => $id,
						'message' => $message,
						'date' => $date
					));

					echo "<div class='hidden-success'>Success</div>";
				}
				catch(PDOException $e) {
					echo $e->getMessage();

				}
			}

	}
?>
