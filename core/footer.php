	<div id='footer'>
		<div class='container'>
			<div class='about'>
				<h4>About us</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea </p>
			</div>

			<div class='social'>
				<h4>Social networks</h4>
				<ul>
					<li><a href=''>Facebook</a></li>
					<li><a href=''>Twitter</a></li>
					<li><a href=''>Instagram</a></li>
					<li><a href=''>Youtube</a></li>
				</ul>
			</div>

			<div class='contact'>
				<h4>Contacts</h4>
				<ul>
					<li><a href=''>E-mail: demo@demo.com</a></li>
					<li><a href=''>Skype: demo</a></li>
					<li><a href=''>Discord: #4416demo</a></li>
				</ul>
			</div>

			<div class='lowerFooter'>
				<p>2019 &copy; Team-finder</p>
			</div>
		</div>
	</div>

	<!-- Navigation javascript -->

	<!-- Code from: https://www.w3schools.com/howto/howto_js_topnav_responsive.asp -->
	<script>
		function myFunction() {
			var x = document.getElementById("mainNav");
			if (x.className === "topnav") {
				x.className += " responsive";
			} else {
				x.className = "topnav";
			}
		}
	</script>

	<script src='assets/js/custom.js'></script>

	<!-- Login ajax -->
	<script>
			$(document).on('submit', '#login-form', function() {

				$(this).find("input[type='submit']").prop('disabled',true);

				setTimeout(function(){
					$("input[type='submit']").removeAttr("disabled");
				}, 1000);

			  $.post('login.php', $(this).serialize(), function(data) {
				$(".message-login").html(data);

				if(data === "<div class='hidden-success'>Success</div>"){
					$("#login-form").trigger('reset');
					location.reload(false);
				}

			  });
			  return false;
			});
		  </script>

			<!-- Register ajax -->

		  <script>
			$(document).on('submit', '#register-form', function() {

				$(this).find("input[type='submit']").prop('disabled',true);

				setTimeout(function(){
					$("input[type='submit']").removeAttr("disabled");
				}, 1000);

			  $.post('register.php', $(this).serialize(), function(data) {
				$(".message-register").html(data);

				if(data === "<div class='hidden-success'>Success</div>"){
					$("#register-form").trigger('reset');
					location.reload(false);
				}

			  });
			  return false;
			});
		  </script>

			<!-- Forgot password ajax -->

		  <script>
			$(document).on('submit', '#forgot-form', function() {

				$(this).find("input[type='submit']").prop('disabled',true);

				setTimeout(function(){
					$("input[type='submit']").removeAttr("disabled");
				}, 1000);

			  $.post('forgot.php', $(this).serialize(), function(data) {
				$(".message-forgot").html(data);

				if(data === "<span>Reset link successfully sent! Please check your email!</span>"){
					$("#forgot-form").trigger('reset');
					location.reload(false);
				}

			  });
			  return false;
			});
		  </script>

			<!-- Add message ajax -->

		  <script>
			$(document).on('submit', '#add-form', function() {
				$(this).find("input[type='submit']").prop('disabled',true);

				setTimeout(function(){
					$("input[type='submit']").removeAttr("disabled");
				}, 1000);

				$.post('add.php', $(this).serialize(), function(data) {
					$(".message-add").html(data);

					if(data === "<div class='hidden-success'>Success</div>"){
						$("#add-form").trigger('reset');
						location.reload(false);
					}
				});
				return false;
			});
		  </script>

	</body>
</html>
