
<!DOCTYPE html>
<html lang='en'>

<head>
	<meta charset='utf-8' />
	<meta name='keywords' content='' />
	<meta name='description' content='' />
	<meta name='author' content='' />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title>Team-Finder - Where alike minds collide</title>
	<link rel="shortcut icon" type="image/png" href="favicon.ico"/>
	<link rel='stylesheet' href='styles.css' />
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script src='assets/js/custom.js'></script>
</head>

<body>
	<header id="mainHeader">
		<div class='container'>

			<!-- Logo -->
			<a href='index.php' class='logo left'>
				<img src='assets/images/logo.png' alt='Team-finder'/>
				<p><span>Team-Finder</span> <br /> Where alike minds collide</p>
			</a>

			<div class='userArea right'>

				<?php
					if(isset($_SESSION['username'])){
				?>

					<a href='profile.php' class='button left green login'>
						<span><i class="fas fa-users"></i></span>
						<span><?php echo $_SESSION['username']; ?> </span>
					</a>

				<?php
					} else {
				?>

          <span class='main-nav'>
						<a href='#' class='button left green login'>
							<span><i class="fas fa-users"></i></span>
							<span class='cd-signin'>User area</span>
						</a>
          </span>

				<?php
					}
				?>
				<div class='clear'></div>
			</div>
		</div>
	</header>

<?php if(isset($_SESSION['username'])){ ?>

    <!-- Add channel modal -->

	<div class="cd-user-modal">
        <div class="cd-user-modal-container">

            <!-- Add channel -->

            <div id="cd-add">
                <h4>Create request</h4>
                <form method="post" class="cd-form" id='add-form'>
                    <select class='select' name='gameID'>
                        <option selected  disabled>Select game...</option>
                        <?php
                            try {
                                $stmt = $connect->prepare('SELECT * FROM games');
                                $stmt->execute();
                                $data = $stmt->fetchAll();

                                foreach ($data as $game){
                                    echo '<option value="'.$game['game_id'].'">'.$game['name'].'</option>';
                                }

                            } catch(PDOException $e) {
                                $err = $e->getMessage();
                            }
                        ?>
                    </select>

										<select class='select' name='chosenTime'>
                        	<option selected  disabled>Choose expiry time...</option>
													<option value="1">1 hour</option>
													<option value="6">6 hours</option>
													<option value="12">12 hours</option>
													<option value="24">1 day</option>
													<option value="168">7 days</option>
                    </select>

                    <p class="fieldset">
                        <input type="checkbox" name="dcts" value="yes"> Require Discord/Teamspeak
                    </p>

                    <p class="fieldset">
                        <span><input type="checkbox" name="adult" value="yes"> Only 18+ years or above</span>
                    </p>

                    <p class="fieldset">
                        <span><input type="checkbox" name="mic" value="yes"> Require microphone</span>
                    </p>

										<p class="fieldset">
                        <label class="image-replace cd-username" for="discord">Discord</label>
                        <input name='discord' class="full-width has-padding has-border" id="discord" type="text" placeholder="Discord"  autocomplete="off">
                    </p>

										<p class="fieldset">
                        <label class="image-replace cd-username" for="skype">Skype</label>
                        <input name='skype' class="full-width has-padding has-border" id="skype" type="text" placeholder="Skype"  autocomplete="off">
                    </p>

										<p class="fieldset">
                        <label class="image-replace cd-username" for="steam">Steam</label>
                        <input name='steam' class="full-width has-padding has-border" id="steam" type="text" placeholder="Steam"  autocomplete="off">
                    </p>

                    <div class="g-recaptcha" data-sitekey="6Lc7caMUAAAAAKeGR7HGWF-tzO5G56X0_cfA7Gzj"></div>

                    <p class="fieldset">
                        <input name='submitChannel' class="full-width blue" type="submit" value="Add game">
                    </p>

                    <span class="message-add"></span>
                </form>
            </div>

            <a href="#0" class="cd-close-form">Leave</a>
        </div>
    </div>
<?php } ?>

<?php if(empty($_SESSION['username'])){ ?>

    <!-- Add channel modal -->

    <div class="cd-user-modal">
        <div class="cd-user-modal-container">
            <ul class="cd-switcher">
                <li><a href="#0">Login</a></li>
                <li><a href="#0">Register</a></li>
            </ul>

            <!-- Login form -->

            <div id="cd-login">
                <form method="post" class="cd-form" id='login-form'>
                    <p class="fieldset">
                        <label class="image-replace cd-username" for="signin-username">Username</label>
                        <input name='username' class="full-width has-padding has-border" id="signin-username" type="text" placeholder="Username"  autocomplete="off">
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-password" for="signin-password">Password</label>
                        <input name='password' class="full-width has-padding has-border" id="signin-password" type="password"  placeholder="Password"  autocomplete="off">
                    </p>

                    <div class="g-recaptcha" data-sitekey="6Lc7caMUAAAAAKeGR7HGWF-tzO5G56X0_cfA7Gzj"></div>

                    <p class="fieldset">
                        <input name='login' class="full-width green" type="submit" value="Login!">
                    </p>

                    <span class="message-login"></span>
                </form>

                <p class="cd-form-bottom-message"><a href="#0">Forgot your password?</a></p>
            </div>

            <!-- Register form -->

            <div id="cd-signup">
                <form method="post" class="cd-form" id='register-form'>
                    <p class="fieldset">
                        <label class="image-replace cd-username" for="signup-username">Username</label>
                        <input name='username' class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username" value="<?php if(isset($_POST['username'])) echo $_POST['username'] ?>" autocomplete="off">
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-email" for="signup-email">E-mail address </label>
                        <input name='email' class="full-width has-padding has-border" id="signup-email" type="email" placeholder="Email address">
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-password" for="signup-password">Password</label>
                        <input name='password' class="full-width has-padding has-border" id="signup-password" type="password"  placeholder="Password" value="<?php if(isset($_POST['password'])) echo $_POST['password'] ?>" autocomplete="off">
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-password" for="signup-rpassword">Repeat password</label>
                        <input name='rpassword' class="full-width has-padding has-border" id="signup-rpassword" type="password"  placeholder="Repeat password" autocomplete="off">
                    </p>

                    <div class="g-recaptcha" data-sitekey="6Lc7caMUAAAAAKeGR7HGWF-tzO5G56X0_cfA7Gzj"></div>

                    <p class="fieldset">
                        <input name='register' class="full-width blue" type="submit" value="Register!">
                    </p>

                    <span class="message-register"></span>
                </form>
            </div>

            <!-- Reset password  -->

            <div id="cd-reset-password">
                <p class="cd-form-message">Please enter your email address to reset your password.</p>

                <form method="post" class="cd-form" id='forgot-form'>
                    <p class="fieldset">
                        <label class="image-replace cd-email" for="reset-email">Email address</label>
                        <input name='email' class="full-width has-padding has-border" id="reset-email" type="email" placeholder="Email address">
                    </p>

                    <div class="g-recaptcha" data-sitekey="6Lc7caMUAAAAAKeGR7HGWF-tzO5G56X0_cfA7Gzj"></div>

                    <p class="fieldset">
                        <input name='reset' class="full-width has-padding green" type="submit" value="Reset my password!">
                    </p>

                    <span class="message-forgot"></span>
                </form>

                <p class="cd-form-bottom-message"><a href="#0">Go back</a></p>
            </div>

            <a href="#0" class="cd-close-form">Exit</a>
        </div>
    </div>
<?php } ?>

<!-- Navigation code -->
<!-- Code from: https://www.w3schools.com/howto/howto_js_topnav_responsive.asp -->

<nav class="topnav" id="mainNav">
    <div class='container'>
        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        <i class="fa fa-bars"></i>
    </a>

        <a href="#" class="active">Home page</a>
        <a href="#">News</a>
        <a href="#">Contacts</a>
        <a href="#">About us</a>
    </div>
</nav>
