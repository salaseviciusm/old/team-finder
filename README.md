# HTML5/CSS3 One page studio template
Screenshots:
* [Screenshot of index page](screenshot-index.png)
* [Screenshot of chat page](screenshot-chat.png)

## Built With

HTML5/CSS3 <br>
Javascript/jQuery/Ajax
PHP/MYSQL

## Authors

* **Mantas Salasevicius** - *Developer* - [Portfolio](https://mantas.dev), [Gitlab](https://gitlab.com/mantas.dev)

## Date of development

2018

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details