<?php
/* Require main connection file */
require 'config.php';

/* If user logged in, don't allow access to this page */

if(isset($_SESSION['username'])){
   header("Location:index.php");
}

/* If token is empty don't allow access to this page */

if($_GET['token'] == '') {
   header("Location:index.php");
}

if(isset($_POST['reset'])){

			$token = $_GET['token'];

			try {
				$stmt = $connect->prepare('SELECT token FROM users WHERE token = :token');
				$stmt->execute(array(
					'token' => $token
				));

				$data = $stmt->fetch(PDO::FETCH_ASSOC);

				if($token == $data['token']){

					$password = $_POST['password'];
					$rpassword = $_POST['rpassword'];
					$hash = password_hash($password, PASSWORD_DEFAULT);
					$response = $_POST["g-recaptcha-response"];

					if($password == '' || $rpassword == '') {
						$err = 'Complete all fields!';
					} else {
						if($password == $rpassword) {
							if($response == ''){
								$err = 'Complete captcha!';
							} else {
								$stmt = $connect->prepare('UPDATE users SET password = :password WHERE token = :token');
								$stmt->execute(array(
									'password' => $hash,
									'token' => $token
								));

								$stmt = $connect->prepare('UPDATE users SET token = "" WHERE token = :token');
								$stmt->execute(array(
									'token' => $token
								));

                                header("Location:index.php");
							}
						} else {
							$err = 'Passwords are different!';
						}
					}

				} else {
					 header("Location:index.php");
				}
			}
			catch(PDOException $e) {
				$err = $e->getMessage();
			}
}
?>

<?php include "core/header.php"; ?>

    <section id='mainContent'>
		<div class='container'>
            <form method="post" class="cd-form">
                <p class="fieldset">
                    <label class="image-replace cd-password" for="signin-password">New password</label>
                    <input name="password" class="full-width has-padding has-border" id="signin-password" type="password"  placeholder="New password" autocomplete="off">
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-password" for="signin-password">Repeat password</label>
                    <input name="rpassword" class="full-width has-padding has-border" id="signin-password" type="password"  placeholder="Repeat password" autocomplete="off">
                </p>

                <div class="g-recaptcha" data-sitekey="6Lc7caMUAAAAAKeGR7HGWF-tzO5G56X0_cfA7Gzj"></div>

                <p class="fieldset">
                    <input name="reset" class="full-width green" type="submit" value="Reset password">
                </p>

                  <span class="message-login"><?php echo $err; ?></span>
            </form>
		</div>
    </section>

<?php include "core/footer.php"; ?>
