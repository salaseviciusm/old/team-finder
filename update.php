<?php

/* Require main connection file */
require 'config.php';

if(isset($_POST['update'])) {

	// Get data

	$username = $_POST['username'];
	$password = $_POST['password'];
	$rpassword = $_POST['rpassword'];
	$email = $_POST['email'];
	$hash = password_hash($password, PASSWORD_DEFAULT);
	$response = $_POST["g-recaptcha-response"];


	if($username == '' || $password == '' || $rpassword == '' || $email == '') {
		$err = 'Complete all fields!';
	} else {
		if($password == $rpassword) {
			if($response == ''){
				$err = 'Check captcha!';
			} else {
				try {
					$stmt = $connect->prepare('UPDATE users SET username = :username, email = :email, password = :password');
					$stmt->execute(array(
						':username' => $username,
						':email' => $email,
						':password' => $hash
						));

					$_SESSION['username'] = $username;
					$_SESSION['email'] = $email;
					$_SESSION['password'] = $hash;

					$err = "Information updated succesfully!";
				}
				catch(PDOException $e) {
					$err = $e->getMessage();
				}
			}
		} else {
			$err = 'Passwords are different!';
		}
	}
}

?>
