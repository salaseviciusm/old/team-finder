<?php

/* Require main connection file */
require 'config.php';

	$id = $_GET['id'];

	if(empty($_SESSION['username'])){
		header('Location: index.php');
	}

	if($id == '') {
		header('Location: index.php');
	}

	if(isset($_GET['id'])) {
		try {
			$stmt = $connect->prepare('SELECT * FROM channels WHERE id = :id');
			$stmt->execute(array(
				':id' => $id
				));
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			if($data == false){
				header("Location: index.php");
			}
		} catch(PDOException $e) {
			$err = $e->getMessage();
		}
	}

try {
			$stmt = $connect->prepare('SELECT * FROM messages INNER JOIN users ON messages.poster_id = users.user_id INNER JOIN channels ON messages.convo_id = channels.id WHERE id = :id ORDER BY messages.message_id DESC');
			$stmt->execute(array(
				':id' => $id
			));

			$data = $stmt->fetchAll();

			foreach ($data as $message) {
				echo "<div class='msg-wrap'><span class='msg-date'>({$message['date']})</span> <span class='msg-user'>{$message['username']}</span>: {$message['message']} </div>";
			}

		} catch(PDOException $e) {
			$err = $e->getMessage();
		}

		?>
