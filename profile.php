<?php
	/* Require main connection file */
	require 'config.php';

	/* Check if user is logged in */
	if(empty($_SESSION['username'])){
		header('Location: login.php');
	}

    if(isset($_POST['update'])) {

	// Get data

	$username = $_POST['username'];
	$password = $_POST['password'];
	$rpassword = $_POST['rpassword'];
	$email = $_POST['email'];
	$hash = password_hash($password, PASSWORD_DEFAULT);
	$response = $_POST["g-recaptcha-response"];
    $id = $_SESSION['memberid'];


	if($username == '' || $password == '' || $rpassword == '' || $email == '') {
		$err = 'Complete all fields!';
	} else {
		if($password == $rpassword) {
			if($response == ''){
				$err = 'Complete captcha!';
			} else {
				try {
					$stmt = $connect->prepare('UPDATE users SET username = :username, email = :email, password = :password WHERE user_id = :id');
					$stmt->execute(array(
						'username' => $username,
						'email' => $email,
						'password' => $hash,
                        'id' => $id
						));

					$_SESSION['username'] = $username;
					$_SESSION['email'] = $email;
					$_SESSION['password'] = $hash;

					$err = "Profile has been updated!";
				}
				catch(PDOException $e) {
					$err = $e->getMessage();
				}
			}
		} else {
			$err = 'Passwords are different!';
		}
	}
}

?>

<?php include "core/header.php"; ?>

<div class='container'>
    <section id='content'>
        <h4>Welcome:  <span><?php echo $_SESSION['username']; ?> (<?php echo $_SESSION['role']; ?>)</span></h4>

        <div class='col-2'>
            Your email address: <span><?php echo $_SESSION['email']; ?></span><br>

            <br>

            <a href="logout.php" class='button left purple login'>
                <span><i class="fas fa-plus"></i></span>
                <p class='cd-add'>Logout</p>
            </a>

            <div class='clear'></div>

        </div>

        <div class='col-2'>

            <form method="post" class="cd-form" id='update-form'>
                <p class="fieldset">
                    <label class="image-replace cd-username" for="signup-username">Username</label>
                    <input name='username' class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username"  autocomplete="off">
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-email" for="signup-email">Email address</label>
                    <input name='email' class="full-width has-padding has-border" id="signup-email" type="email" placeholder="Email address">
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-password" for="signin-password">Password</label>
                    <input name='password' class="full-width has-padding has-border" id="signin-password" type="password"  placeholder="Password"  autocomplete="off">
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-password" for="signin-password">Repeat password</label>
                    <input name='rpassword' class="full-width has-padding has-border" id="signin-password" type="password"  placeholder="Repeat password" autocomplete="off">
                </p>

                <div class="g-recaptcha" data-sitekey="6Lc7caMUAAAAAKeGR7HGWF-tzO5G56X0_cfA7Gzj"></div>

                <p class="fieldset">
                    <input name='update' class="full-width blue" type="submit" value="Update profile!">
                </p>

                <span class="message-update"><?php echo $err; ?></span>
            </form>

        </div>
    </section>
</div>

<?php include "core/footer.php"; ?>
