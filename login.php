<?php

/* Require main connection file */
require 'config.php';

/* Make sure file is not accessible using the link to the page as its only used in ajax calls */

if(isset($_SESSION['username'])){
   header("Location:index.php");
}

if(!isset($_POST['username'])){
	header("Location:index.php");
}

	// Get data from

	$username = $_POST['username'];
	$password = $_POST['password'];
	$response = $_POST["g-recaptcha-response"];

	if($username == '' || $password == '') {
		echo 'Complete all fields!';
	} else {
		if($response == ''){
			echo 'Complete captcha!';
		} else {
			try {
				$stmt = $connect->prepare('SELECT * FROM users WHERE username = :username');
				$stmt->execute(array(
					':username' => $username
					));
				$data = $stmt->fetch(PDO::FETCH_ASSOC);
				if($data == false){
					echo "User with username $username not found.";
				} else {

          // Verify password to encrypt it //
					if(password_verify($password, $data['password'])) {
                        $_SESSION['memberid'] = $data['user_id'];
                        $_SESSION['role'] = $data['role'];
						$_SESSION['username'] = $data['username'];
						$_SESSION['email'] = $data['email'];
						$_SESSION['password'] = $data['password'];

						echo "<div class='hidden-success'>Success</div>";
					} else {
						echo 'Wrong password!';
					}
				}
			} catch(PDOException $e) {
				$err = $e->getMessage();
			}
		}
	}
?>
