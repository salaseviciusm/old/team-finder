<?php
	/* Require main connection file */
	require 'config.php';

	/* Destroy session to log out the user */
	session_destroy();
	header('Location: index.php');
?>
